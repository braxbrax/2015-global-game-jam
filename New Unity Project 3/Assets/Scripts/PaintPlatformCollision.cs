﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PaintPlatformCollision : MonoBehaviour {

	public float Y_SPLAT_OFFSET;
	public List<GameObject> splatTextures;
	public bool Visible = false;

	// Use this for initialization
	void Start () {

		if (Visible)
			VisibilityOn();
        LevelManager ls = GameObject.FindWithTag("Manager").GetComponent<LevelManager>();

        splatTextures = ls.splatTextures;
		
        print(splatTextures.Count);

		if (ls.ShowPlatforms)
			VisibilityOn();
		else
			VisibilityOff();
		
		Y_SPLAT_OFFSET = -0.25f;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.U))
		    VisibilityOn();
		else if (Input.GetKeyDown(KeyCode.I))
		    VisibilityOff();
	}

	void OnTriggerEnter2D(Collider2D paint){
		if (paint.transform.tag == "Paint") {
			SpawnSplat(paint);
			Destroy (paint.transform.gameObject);
			if (!Visible)
				VisibilityOn();
		}
	}

	void VisibilityOn(){
		for (int i = 0; i < this.transform.parent.childCount; i++)
			if (this.transform.parent.GetChild(i).name == "Texture") this.transform.parent.GetChild(i).renderer.active = true;
	}

	void VisibilityOff(){
		for (int i = 0; i < this.transform.parent.childCount; i++)
			if (this.transform.parent.GetChild(i).name == "Texture") this.transform.parent.GetChild(i).renderer.active = false;
	}

	void SpawnSplat(Collider2D paint) {

		Vector3 position = new Vector3 (0.0f, Y_SPLAT_OFFSET, 0.0f);
		position += paint.transform.position;

        Color paintColor = paint.transform.gameObject.renderer.material.color;

		//GameObject splatInstance = (GameObject)(Instantiate(splatTexture, position, Quaternion.identity));
        int randomNum = Random.Range(0, 361);
		GameObject splat = (GameObject)Instantiate(splatTextures[Random.Range(0,5)], position, Quaternion.AngleAxis(randomNum, Vector3.back));
        splat.renderer.material.color = paintColor;
	}
}
