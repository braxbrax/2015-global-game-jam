﻿using UnityEngine;
using System.Collections;

public enum GameState { menu, gameplay, pause };

public class GameManager : MonoBehaviour {
	GameState gameState;



	public GameState CurrentState{
		set{ gameState = value;	}
		get{ return gameState; }
	}

	// Use this for initialization
	void Start () {
		//gameState = GameState.menu;
	}
	
	// Update is called once per frame
	void Update () {
		switch(gameState){
		
			case GameState.menu:
				break;

			case GameState.gameplay:
				break;

		}
	}
}
