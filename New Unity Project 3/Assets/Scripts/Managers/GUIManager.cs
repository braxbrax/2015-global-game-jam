﻿using UnityEngine;
using System.Collections;

public class GUIManager : Singleton<GUIManager>
{
	public Menu mainMenu;
	public Menu gamePlayMenu;
	public Menu pauseMenu;

	private Menu currentMenu;
	private bool isPaused;
	private bool gameStarted;

	#region Initialization

	/// <summary>
	/// Protected constructor that can't be used, forcing this object to be a singleton.
	/// </summary>
	protected GUIManager() {}

	// while the game is starting, loadingMenu is active
	void Awake()
	{
		mainMenu.OnActive();
		currentMenu = mainMenu;
		mainMenu.OnActive();
		isPaused = false;
		gameStarted = false;
	}

	#endregion

	#region Properties

	public bool GameStarted{
		get{return gameStarted;}
		set{gameStarted = value;}
	}

	public bool IsPaused{
		get{return isPaused; }
		set{ isPaused = value; }
	}

	public Menu CurrentMenu
	{
		get { return currentMenu; }

		set
		{
			currentMenu.gameObject.SetActive(false);
			currentMenu.OnScreenExit();
			currentMenu = value;
			currentMenu.gameObject.SetActive(true);
			currentMenu.OnActive();
		}
	}

	#endregion

	/// <summary>
	/// Handles all GUI events and renders the appropriate GUI in the game.
	/// </summary>
	void OnGUI()
	{
		if (CurrentMenu != null)   // If a Menu should currently be displayed
		{
			CurrentMenu.Display();
		}
	}

	void Update(){
		if(Input.GetKeyDown(KeyCode.Escape) && currentMenu == gamePlayMenu){
			CurrentMenu = pauseMenu;
		}
	}
}