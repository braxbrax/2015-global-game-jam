﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : Singleton<LevelManager> {

	public GameObject levelTemplate;
	public float offset = 5;
	public float sectionHeight = 8;
	public GameObject startSection;
	public GameObject checkPoint;
	public List<GameObject> sections;
	private bool spawnedStart = false;
	public bool ShowPlatforms;
	public List<GameObject>splatTextures;

	private GameObject level;

	// Use this for initialization
	void Start () {
		if (ShowPlatforms == null)
			ShowPlatforms = true;
	}


	public GameObject GenerateLevel(Vector3 pos, int numSections){
		GameObject lvlInstance = (GameObject)(Instantiate(levelTemplate, pos, Quaternion.identity));

		int i = 0;

		if(!spawnedStart){
			GameObject lvlSection = (GameObject)(Instantiate(startSection, new Vector3(0, offset, 0), Quaternion.identity));
			lvlSection.transform.parent = lvlInstance.transform;
			i++;
			spawnedStart = true;
		}

		for(i = i; i < numSections; i++){
			if(i%3 == 0){
				GameObject ckpt = (GameObject)(Instantiate(checkPoint, new Vector3(0, sectionHeight*i + offset, 0), Quaternion.identity));
				ckpt.transform.parent = lvlInstance.transform;
			}


			GameObject lvlSection = (GameObject)(Instantiate(sections[(int)Random.Range(0.0f, sections.Count)], new Vector3(0, sectionHeight*i + offset, 0), Quaternion.identity));
			lvlSection.transform.parent = lvlInstance.transform;
		}
		level = lvlInstance;
		return lvlInstance;
	}	

	public void LevelSetActive(bool value){
		level.SetActive(value);
	}

	public void LevelRemove(){
		Destroy(level);
	}
}
