﻿using UnityEngine;
using System.Collections;

public class FairyInput : MonoBehaviour {
	
	public float speed;
	private Vector3 target;
	Animator animator;
	// Use this for initialization
	void Start () {
		target = transform.position;
		//animator = this.transform.GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {
		target = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		target.z = transform.position.z;
		Vector3 distance = target - transform.position;
		speed = distance.magnitude * 10;
		/*if (distance.magnitude > 0.1)
			this.animator.SetBool("Moving", true);
		else
			this.animator.SetBool("Moving", false);
		if (Input.GetMouseButtonDown(0))
			this.animator.SetTrigger("Click");*/
		transform.position = Vector3.MoveTowards (transform.position, target, speed * Time.deltaTime);
	}
}
