﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour
{
	public Vector3 offset;			// The offset at which the Health Bar follows the player.
	public float cameraBuffer; // Distance that player can move around before camera follows
	private bool enabled = false;
	private Vector3 healthOffset;
	private GameObject hud;

	private Transform player;		// Reference to the player.
	
	
	void Awake ()
	{

		
	}

	void OnEnable(){
		enabled = true;
		player = GameObject.FindGameObjectWithTag("Player").transform;
		hud = GameObject.FindGameObjectWithTag("HUD");
	}
	
	void Update ()
	{
		if (enabled){
			// Set the position to the player's position with the offset.
			if (player.position.y >= (camera.pixelRect.y - 3)) {
				Vector3 pos = new Vector3 (0.0f, player.position.y, -10);
				transform.position = pos + offset;

			}
			hud.transform.position = new Vector3(0.5f, transform.position.y, 0.0f);
			print (hud.transform.position.y);
		}

		


	}
}
