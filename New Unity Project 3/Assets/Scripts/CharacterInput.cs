﻿using UnityEngine;
using System.Collections;

public class CharacterInput : MonoBehaviour {

	Vector2 leftForce = new Vector2(-10, 0);
	Vector2 rightforce = new Vector2(10, 0);
	float jumpSpeed = 7.7f;
	float runSpeed = 1;
	Vector2 leftSide = new Vector2(0, 0);
	Vector2 rightSide = new Vector2(0, 0);
	float airSpeed = 0.5f;
	float maxSpeed = 5;
	int jumps = 0;
	int facing = -1;
	Vector2 down = new Vector2(0, 0);
	Animator animator;
	public Vector3 currentCheckpoint = new Vector3(0.0f, 0.0f, 0.0f);

	// Use this for initialization
	void Start () {
		animator = this.transform.GetComponent<Animator>();
		down.y = this.collider2D.bounds.size.y;
		leftSide.x = -this.collider2D.bounds.size.x;
		rightSide.x = -leftSide.x;
		//this.rigidbody2D.drag = 1f;

	}
	
	// Update is called once per frame
	void Update () {
		bool collide = false;
		Vector2 position = this.rightSide;
		if (this.rigidbody2D.velocity.y <= 0.1){
			if (!checkGround (this.rightSide)){
				checkGround (this.leftSide);
			}
		}
		if (Input.GetKeyDown("space") || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)){
			if (this.jumps < 2){
				this.rigidbody2D.velocity = new Vector2(this.rigidbody2D.velocity.x, jumpSpeed/this.rigidbody2D.mass - 1 * this.jumps);
				this.jumps ++;
				if (animator)
					animator.SetTrigger("Jump");
			}

		}
		bool moving = false;
		if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)){
			this.rigidbody2D.AddForce(this.jumps == 0 ? this.leftForce : this.leftForce * airSpeed);
			moving = true;
			if (facing > 0)
				flip();
		}
		if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)){
			this.rigidbody2D.AddForce(this.jumps == 0 ? this.rightforce : this.rightforce * airSpeed);
			moving = true;

			if (facing < 0)
				flip();
		}
		Vector2 zero = Vector2.zero;
		if (this.rigidbody2D.velocity.x > runSpeed)
			zero.x = (runSpeed - this.rigidbody2D.velocity.x) / this.rigidbody2D.mass;
		else if (this.rigidbody2D.velocity.x < -runSpeed)
			zero.x = (-runSpeed - this.rigidbody2D.velocity.x) / this.rigidbody2D.mass;
		this.rigidbody2D.AddForce(zero);

		if (animator)
			animator.SetBool("Walking", moving);

	}

	bool checkGround(Vector2 position){
		RaycastHit2D[] hits = Physics2D.RaycastAll(this.rigidbody2D.position + position - down, down, 0.025f);
		for (int i = 0; i < hits.Length; i++){ 
			if (hits[i].collider.gameObject.tag == "Platform" || hits[i].collider.gameObject.tag == "platform"){
				this.jumps = 0;
				return true;
			}
		}
		return false;
	}

	public void jump(){
		jumps ++;
		audio.Play();
	}

	void flip(){
		facing = -facing;

		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	public void setCurrentCheckpoint(Vector3 location){

		currentCheckpoint = location;
		currentCheckpoint.x += 0.3f;
		currentCheckpoint.y += 0.3f;
	}

	public Vector3 getCurrentCheckpoint(){
		
		return currentCheckpoint;
	}
}
