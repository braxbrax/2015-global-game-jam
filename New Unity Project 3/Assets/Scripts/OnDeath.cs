﻿using UnityEngine;
using System.Collections;

public class OnDeath : MonoBehaviour {

	public GameObject Player;

	// Use this for initialization
	void Start () {
	
		//if lives == 0
		//game over
		//play again?

		//else if lives > 0
		//spawn character at last checpoint
		//reset camera to checkpoint's x, y
		//lives--


	}
	
	// Update is called once per frame
	void Update () {
		if (Player.GetComponentInChildren<SpriteRenderer>().renderer.isVisible == false) {
			Player.SetActive(false);
			Vector3 spawn = Player.rigidbody2D.GetComponent<CharacterInput>().getCurrentCheckpoint();
			Player.transform.position.Set(spawn.x, spawn.y, spawn.z);
		}
	}
}
