﻿using UnityEngine;
using System.Collections;

public class PlatformFadeManager : MonoBehaviour
{

    public bool visible;
    public float maxTime;
    public float timerSpeed;
    public bool isRandom;
    private float currentTime;
    public GameObject texture;

    // Use this for initialization
    void Start()
    {
        currentTime = maxTime;
        if (isRandom)
        {
            maxTime = Random.Range(0.6f, 1.1f);
            timerSpeed = Random.Range(0.01f, 0.04f);

            int randomBool = Random.Range(0,10);
            if (randomBool % 2 == 0)
            {
                visible = true;
            }
            else
            {
                visible = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= timerSpeed;

        if (currentTime <= 0)
        {
            visible = !visible;

            if (visible == true)
            {
                gameObject.collider2D.enabled = true;
                texture.renderer.active = true;
            }
            else
            {
                gameObject.collider2D.enabled = false;
                texture.renderer.active = false;
                
            }
            currentTime = maxTime;
        }
    }
}