﻿using UnityEngine;
using System.Collections;

public class PlatformMoveManager : MonoBehaviour {

    public int distance;
    private int halfDistance;
    public float angle;
    private float oppositeAngle;
    public float speed;
    private Vector3 startPosition;
    public bool flippedMotion;

    private Vector3 finishPos;
    private Vector3 flippedFinishPos;

	// Use this for initialization
	void Start () {
        startPosition = transform.position;
        halfDistance = distance / 2;
        oppositeAngle = (angle - 180) * Mathf.Deg2Rad;
        angle *= Mathf.Deg2Rad;


        //calculate the finish positions for the debug lines
        Vector3 oppositeAngleVec = new Vector3(Mathf.Cos(oppositeAngle), Mathf.Sin(oppositeAngle), transform.position.z);
        oppositeAngleVec *= halfDistance;
        flippedFinishPos = transform.position + oppositeAngleVec;

        Vector3 angleVec = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), transform.position.z);
        angleVec *= halfDistance;
        finishPos = transform.position + angleVec;

	}
	
	// Update is called once per frame
	void Update () {
        var heading = transform.position - startPosition;

        drawDebugLine();

        //check if the platform is still in range of the starting position
        if (heading.sqrMagnitude > halfDistance * halfDistance)
        {
            //flip the direction if it is
            flippedMotion = !(flippedMotion);
        }

        Vector3 newPosition = transform.position;

        switch (flippedMotion)
        {
            case true:
                //calculate the future position based off the opposite angle and multiply by speed
                Vector3 oppositeAngleVec = new Vector3(Mathf.Cos(oppositeAngle), Mathf.Sin(oppositeAngle), transform.position.z);
                oppositeAngleVec *= speed;

                //adjust the position
                newPosition.x += oppositeAngleVec.x;
                newPosition.y += oppositeAngleVec.y;

                break;
            case false:

                //calculate the future position based off the angle  and multiply by speed
                
                Vector3 angleVec = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), transform.position.z);
                angleVec *= speed;

                //adjust the position
                newPosition.x += angleVec.x;
                newPosition.y += angleVec.y;

                break;
        }

        //print("Flipped Motion: " + flippedMotion + "\nNewPosition: " + newPosition);

        transform.position = newPosition;
	    
	}

    //draw the path
    void drawDebugLine()
    {
        Debug.DrawLine(startPosition, finishPos, Color.red);
        Debug.DrawLine(startPosition, flippedFinishPos, Color.red);
    }
}
