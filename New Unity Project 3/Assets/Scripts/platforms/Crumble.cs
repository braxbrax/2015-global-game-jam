﻿using UnityEngine;
using System.Collections;

public class Crumble : MonoBehaviour {

	public GameObject platform;
	public Vector3 myPosition;
    public bool isRandom;
    public float waitUpTime;
    public float waitDownTime;
	// Use this for initialization
	void Start () {
		myPosition = transform.position;
        if (isRandom)
        {
            waitUpTime = Random.Range(0.3f, 1f);
            waitDownTime = Random.Range(1f, 4f);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D player){
		if (player.transform.tag == "Player") {
			print ("Player");
            if (player.transform.rigidbody2D.velocity.y <= 0)
            {
                StartCoroutine(WaitAndLoad(myPosition));
            }
		}
	}

	IEnumerator WaitAndLoad(Vector3 myPosition){
        yield return new WaitForSeconds(waitUpTime);
		gameObject.collider2D.enabled = false;
		rigidbody2D.isKinematic = false;
		rigidbody2D.gravityScale = 2;
        yield return new WaitForSeconds(waitDownTime);
		transform.position = myPosition;
		gameObject.collider2D.enabled = true;
		rigidbody2D.isKinematic = true;
	}
}
