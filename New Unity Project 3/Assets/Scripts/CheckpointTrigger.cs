﻿using UnityEngine;
using System.Collections;

public class CheckpointTrigger : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
	
	}

	void OnCheckpoint(){

		//setCurrentChecpoint ();
	}

	void OnTriggerEnter2D(Collider2D player){
		if (player.transform.tag == "Player") {
			//Transform platform = transform.parent;
			//Physics2D.IgnoreCollision(player.collider2D, platform.collider2D);
			GameObject localPlayer = GameObject.FindWithTag("Player");
			localPlayer.rigidbody2D.GetComponent<CharacterInput>().setCurrentCheckpoint(gameObject.transform.position);
			print (gameObject.transform.position);
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
