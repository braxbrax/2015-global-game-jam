﻿using UnityEngine;
using System.Collections;

public class EnemyLaunch : MonoBehaviour {

	public GameObject water;
	private Vector3 target;
	public bool canSee;
	private float timer;
	public float maxTime = 30.0f;
	public float timerSpeed = 0.5f;
	private float currentTime;


	// Use this for initialization
	void Start () {
		print ("AAAAAAAAAAAAAAAH");
		target = transform.position;
		currentTime = maxTime;
	}
	
	// Update is called once per frame
	void Update () {
		if (renderer.isVisible == true) {
			currentTime -= timerSpeed;

			if (currentTime <= 0){
				Attack();
				currentTime = maxTime;
			}
		}
	}

	private void Attack(){
						GameObject newWater = (GameObject)Instantiate (water, target, Quaternion.identity);
						newWater.rigidbody2D.velocity = transform.TransformDirection (Vector2.right * 4);
						if (newWater.renderer.isVisible == false) {
							Destroy (newWater);
				}
		}
		
}
