﻿using UnityEngine;
using System.Collections;

public class DropPaint : MonoBehaviour {

	private Vector3 target;
	public GameObject paint;
	public float bucket = 100;
	GameObject rec;
	private GameObject player;
	float mfX;
	float axis;
	float initialScale;
	
	// Use this for initialization
	void Start () {
		rec = GameObject.FindGameObjectWithTag("healthBar");
		player = GameObject.FindGameObjectWithTag("Player");
		axis = rec.transform.position.y;
		initialScale = rec.transform.localScale.x;
		//rec.transform.localScale = new Vector3 (10, 2, 0);
		target = transform.position;
		if (bucket != 0) {
			mfX = rec.transform.renderer.bounds.size.x / bucket / 2.0f;
			print ("mfx: " + mfX + "bounds: " + rec.transform.renderer.bounds.size.x);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0)) {
			target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			target.z = transform.position.z;
			if (bucket != 0){
					Vector3 v3Scale = rec.transform.localScale;
					rec.transform.localScale = new Vector3(bucket/100 * initialScale , v3Scale.y , v3Scale.z);
					rec.transform.position = new Vector3(rec.transform.position.x - mfX, rec.transform.position.y, 0);

				GameObject p = (GameObject)Instantiate(paint, target, Quaternion.identity);
				Physics2D.IgnoreCollision(p.collider2D, player.collider2D);
				bucket -= 1;
			}
		}
		/*if (Input.GetMouseButtonDown (1)) {
			target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			target.z = transform.position.z;
			if (bucket != 0){
				Vector3 v3Scale = rec.transform.localScale;
				rec.transform.localScale = new Vector3(v3Scale.x - 0.5f , v3Scale.y , v3Scale.z);
				rec.transform.position = new Vector3(mfX + rec.transform.localScale.x / 2.0f, axis, 0);
				GameObject newPaint = (GameObject)Instantiate(paint, target, Quaternion.identity);
				newPaint.rigidbody2D.gravityScale = 0;
				newPaint.rigidbody2D.velocity = newPaint.transform.TransformDirection(Vector2.right * 6);
				bucket -= 1;
			}
		}*/
	}


}
