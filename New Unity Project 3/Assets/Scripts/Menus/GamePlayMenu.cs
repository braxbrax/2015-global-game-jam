﻿using UnityEngine;
using System.Collections;

public class GamePlayMenu : Menu {
	public GameObject RunnerPrefab;
	private GameObject runner;
	public GameObject FairyPrefab;
	private GameObject fairy;
	public GameObject HUDPrefab;
	private GameObject hud;
	public int lives = 3;
	public int death = 0;


	public GameObject Runner{
		set{runner = value;}
		get{return runner;}
	}

	public GameObject Fairy{
		get{return fairy;}
		set{fairy = value;}
	}

	/// <summary>
	/// Displays all of the GUI components for this MainMenu.
	/// </summary>
	public override void Display()
	{
		
		// Main Menu Window
		GUI.BeginGroup(new Rect(0.0f, 0.0f, Screen.width, Screen.height));

		
		GUI.EndGroup();
	}

	public override void OnActive(){
		print ("Game Play Screen activated");
		if(!GUIManager.Instance.GameStarted){
		GUIManager.Instance.GameStarted = true;
		}else if(GUIManager.Instance.IsPaused){
			runner.SetActive(true);
			fairy.SetActive(true);
		}

		if(runner == null || fairy == null){
			runner = (GameObject)(Instantiate(RunnerPrefab, new Vector2(0, -2), Quaternion.identity));
			fairy = (GameObject)(Instantiate (FairyPrefab));

			hud = (GameObject)(Instantiate(HUDPrefab));
			LevelManager.Instance.GenerateLevel(Vector3.zero, 50);
		}

		if (death != lives) {
						if (runner.activeSelf == false) {
								runner.SetActive (true);
								death += 1;
						}
				}
		GUIManager.Instance.IsPaused = false;
		GameObject.FindGameObjectWithTag("MainCamera").GetComponent<FollowPlayer>().enabled = true;
	}

	public override void OnScreenExit(){
		runner.SetActive(false);
		fairy.SetActive(false);
	}
}
