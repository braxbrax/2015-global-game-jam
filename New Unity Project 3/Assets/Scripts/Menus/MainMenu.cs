using UnityEngine;
using System.Collections;

public class MainMenu : Menu
{
	public GameObject dancingRunner;
	public GameObject dancingFairy;

    public Texture title;
	public Texture fairyControls;
	public Texture runnerControls;
	public Texture play;

	private GameObject drunner;
	private GameObject dfairy;

	void Start(){

	}

	/// <summary>
	/// Displays all of the GUI components for this MainMenu.
	/// </summary>
	public override void Display()
	{

		// Main Menu Window
       GUI.BeginGroup(new Rect(0.0f, 0.0f, Screen.width, Screen.height));
		
       GUI.DrawTexture(new Rect(Screen.width /2 - (title.width*1.5f)/2, Screen.height/8, title.width*1.5f, title.height*1.5f), 
		                	title,
		                	ScaleMode.ScaleToFit, 
		                	true, 
		                	0);

		if(GUI.Button(new Rect(Screen.width /2 - play.width/2, Screen.height/1.5f, play.width, play.height), 
		              play, GUIStyle.none)){
			GUIManager.Instance.CurrentMenu = GUIManager.Instance.gamePlayMenu;
		}

		GUI.DrawTexture(new Rect(Screen.width - (Screen.width/8) - fairyControls.width, Screen.height/2.2f, fairyControls.width, fairyControls.height), 
		                fairyControls,
		                ScaleMode.ScaleToFit, 
		                true, 
		                0);

		GUI.DrawTexture(new Rect(Screen.width/8, Screen.height/2.2f, runnerControls.width, runnerControls.height), 
		                runnerControls,
		                ScaleMode.ScaleToFit, 
		                true, 
		                0);

		GUI.EndGroup();
	}

	public override void OnActive(){
		if(GUIManager.Instance.GameStarted){
			LevelManager.Instance.LevelRemove();
		}

		print ("Main menu Activated");
		if(drunner == null || dfairy == null){
			drunner = (GameObject)(Instantiate(dancingRunner, new Vector3(-5.0f, 3.5f, 0.0f), Quaternion.identity));		
			dfairy = (GameObject)(Instantiate (dancingFairy, new Vector3(10.0f, 3.5f, 0.0f), Quaternion.identity)); 
		}
		drunner.SetActive(true);
		dfairy.SetActive(true);
		GUIManager.Instance.IsPaused = false;

	}

	public override void OnScreenExit(){

		this.drunner.SetActive(false);
		this.dfairy.SetActive(false);

		if(drunner != null && dfairy != null){
			drunner.SetActive(false);
			dfairy.SetActive(false);
		}

	}
}