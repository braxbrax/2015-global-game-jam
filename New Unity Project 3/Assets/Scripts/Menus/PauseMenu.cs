﻿using UnityEngine;
using System.Collections;

public class PauseMenu : Menu {
	public Texture resume;
	public Texture mainmenu;
	public Texture background;

	void Start(){
		
	}
	
	/// <summary>
	/// Displays all of the GUI components for this MainMenu.
	/// </summary>
	public override void Display()
	{
		
		// Main Menu Window
		GUI.BeginGroup(new Rect(0.0f, 0.0f, Screen.width, Screen.height));

		GUI.DrawTexture(new Rect(Screen.width /2 - background.width/2, Screen.height/4.4f, background.width, background.height), 
		                background,
		                ScaleMode.ScaleToFit, 
		                true, 
		                0);

		
		if(GUI.Button(new Rect(Screen.width /2 - resume.width/2, Screen.height/3.5f, resume.width, resume.height), 
		              resume, GUIStyle.none)){
			GUIManager.Instance.CurrentMenu = GUIManager.Instance.gamePlayMenu;
		}

		if(GUI.Button(new Rect(Screen.width /2 - mainmenu.width/2, Screen.height/2.0f, mainmenu.width, mainmenu.height), 
		              mainmenu, GUIStyle.none)){
			GUIManager.Instance.CurrentMenu = GUIManager.Instance.mainMenu;
		}

		GUI.EndGroup();
	}
	
	public override void OnActive(){
		print ("Pause Menu activated");
		GUIManager.Instance.IsPaused = true;
		LevelManager.Instance.LevelSetActive(false);
	}
	
	public override void OnScreenExit(){
		LevelManager.Instance.LevelSetActive(true);
	}
	
}
